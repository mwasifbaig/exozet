<?php
/**
 * Created by PhpStorm.
 * User: wasif baig
 * Date: 17/03/2018
 * Time: 21:15
 */

namespace AppBundle\Utils;

use AppBundle\Utils\Functions;
use GuzzleHttp\Exception\ClientException;
use Unirest\Exception;

/*
 * GitHub Api Class
 *
 */

class GitHub
{

    protected $apiUrl = 'https://api.github.com';
    protected  $client;

    /*
     * Initialize member variables
     */
    function __construct()
    {

        $this->client = new \GuzzleHttp\Client(['verify' => false,'http_errors' => false]);
    }


    /*
     * getData
     * fetch user data and aggregate
     *
     * @param string $username
     * @return Array $user
     */
    public function getData($username)
    {


        $apiUrl = $this->apiUrl . "/users/$username";
        $user = $this->callApi($apiUrl);


        $repos = Array();
        if( isset($user['repos_url']) )
        {
            $repos = $this->callApi($user['repos_url']);
            $repos = Functions::arraySort($repos, 'watchers', SORT_DESC);
        }
        $user['repositories'] = $repos;



        $languages_ratio = array();
        if( count($repos) > 0)
        {
            $languages = array();
            foreach ($repos as $repo)
            {
                if($repo['language'] != null) {

                    if( !isset($languages[$repo['language']]) )
                        $languages[$repo['language']] = 1;
                    else
                        $languages[$repo['language']] += 1;
                }
            }

            foreach($languages as $lang=>$count)
            {
                $ratio = round($count/count($repos) * 100,2);
                $temp = array('name'=>$lang, 'ratio'=>$ratio);

                array_push($languages_ratio,$temp);
            }
        }

        $user['languages'] = $languages_ratio;


        return $user;

    }

    /*
     * callApi
     * call rest API and convert json into array
     *
     * @param string $apiUrl
     * @return Array $response
     */

    private function callApi($apiUrl)
    {


        try{
            $res = $this->client->request('GET', $apiUrl);

            if($res->getStatusCode() == 404)
            {
                throw new Exception($res->getReasonPhrase());
            }


        }
        catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            throw new Exception($responseBodyAsString);


        }



        $response = json_decode($res->getBody(),true);

        return $response;
    }





}