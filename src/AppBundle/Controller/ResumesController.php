<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Utils\GitHub;
use AppBundle\Forms\GetUserName;
use Unirest\Exception;


class ResumesController extends Controller
{
    /**
     * get username from input form
     *
     * @param  Request $request
     */
    public function getUserNameFormAction(Request $request)
    {

        $form = $this->createForm(GetUserName::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $formData = $form->getData();

            return $this->redirectToRoute('resume',array('username'=>$formData->getName()));

        }


        return $this->render('user/getUserName.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * Show resume page
     *
     * @param string $username
     * @param GitHub $GitHub
     */
    public function showResumeAction(GitHub $GitHub, $username)
    {

        if( empty($username) )
            return new Response('Username is empty');

        try{

            $data = $GitHub->getData($username);

            return $this->render('user/resume.html.twig', array(
                'data' => $data,
            ));

        }catch (Exception $ex)
        {

            return new Response($ex->getMessage());
        }

    }



}
